Title: Installation
Date: 2017-03-11 10:02
Slug: install
Summary: Installation
toc_run: true
toc_title: Comment installer Kresus&nbsp;?

&#9888; Cette rubrique est en cours de réécriture, il se pourrait que des erreurs
soient présentes. Veuillez nous excuser pour la gêne occasionée pendant ce
temps, et n'hésitez-pas à [proposer vos *merge requests*](https://framagit.org/bnjbvr/kresus.org) pour corriger le tir.

## Avec Docker

### Lancez une image pré-construite

L'image Docker a l'avantage d'inclure une installation de Weboob complète. A
chaque redémarrage de l'image, Weboob essayera de se mettre à jour ; si vous
rencontrez donc un problème de modules, il est recommandé de simplement
redémarrer l'image (un simple `restart` suffit).

L'image pré-construite expose deux volumes de données. Il n'est pas obligatoire
de les monter, bien qu'

- `/home/user/data` contient toutes les données utilisées par Kresus. Il est
  recommandé de monter ce volume, pour éviter des pertes de données entre
  chaque nouvelle instanciation de l'image (par exemple, après une mise à
  jour).
- `/weboob` contient le clône local de Weboob installé au démarrage. L'exposer
  au système local n'est pas obligatoire, mais permet de :
    - mutualiser les répertoires d'installation de Weboob, si plusieurs images
      Kresus sont instanciées. Cela permet notamment des économies d'espace
      disque.
    - mettre en place des `crontab` sur la machine hôte, qui se chargeront de
      mettre à jour Weboob régulièrement (avec un `git pull` depuis le
      répertoire de Weboob sur l'hôte).

La variable d'environnement suivante peut être définie :

- `LOCAL_USER_ID` permet de choisir l'UID de l'utilisateur interne de l'image
  Docker (afin de ne pas faire que kresus tourne en root dans l'image).

Voici un exemple de ligne de commande pour lancer Kresus en production dans une
image Docker, avec le même utilisateur UNIX que l'actuel :

    :::bash
    docker run -p 9876:9876 \
        -e LOCAL_USER_ID=`id -u` \
        -v /opt/kresus/data:/home/user/data \
        -v /opt/kresus/weboob:/weboob \
        -ti -d bnjbvr/kresus

### Construisez l'image vous-même

#### Image stable

L'image correspondant à la dernière version stable de Kresus peut être
téléchargée via le *hub* de Docker. Il s'agit de l'image accessible via
`bnjbvr/kresus`. Il est possible de la reconstruire à la main via la commande
suivante :

    :::bash
    make docker-stable

#### Image nightly

Il existe un Dockerfile pour construire et utiliser la version de développement
de Kresus. Les commandes suivantes permettent de la reconstruire :

    :::bash
    make docker-nightly

## Pré-requis pour les autres installations

Kresus utilise [Weboob](http://weboob.org/) sous le capot, pour se connecter au
site web de votre banque. Vous aurez besoin d'[installer le cœur et les modules
Weboob](http://weboob.org/install) afin que l'utilisateur exécutant Kresus
puisse les utiliser.

Kresus nécessite la dernière version stable de Weboob. Bien que Kresus puisse
fonctionner avec de précédentes versions, les modules des banques peuvent être
obsolètes, et la synchronisation avec votre banque s'en retrouverait
dysfonctionnelle.

## Installation autonome

**AVERTISSEMENT: Il n'y a aucun système d'authentification intégré dans Kresus,
il est donc risqué de l'utiliser tel quel. Choisissez cette option uniquement
si vous savez ce que vous faites et êtes capable de gérer l'authentification
vous-même…**

Cela installera les dépendances, construira le projet et installera le
programme dans le répertoire node.js global. Notez que si cet emplacement est
`/usr/local/bin`, vous devrez probablement lancer cette commande en tant que
root.

## Installation globale

Autrement, si vous souhaitez installer Kresus globalement vous utiliserez :

    :::bash
    make install

Et pourrez ensuite simplement lancer Kresus depuis n'importe quel terminal
depuis n'importe quel répertoire avec :

    :::bash
    kresus

## Installation sur CozyCloud

Si vous possédez déjà une instance Cozy, la meilleure solution (et la
[seule](https://github.com/cozy/cozy-home/issues/789)) est d'installer Kresus
depuis le *marché*.

## Essai local

Installez les dépendances node et construisez les scripts (cela n'installera
pas Kresus globalement) :

    :::bash
    make localrun

## Options au lancement

Notez que quel que soit le processus de lancement choisi (global ou local),
vous pouvez définir plusieurs options :

- le **port** par défaut est 9876. Cela peut être surchargé via la variable
  d'environnement `PORT`.

- le **host** par défaut sur lequel Kresus écoute est `localhost`. Cela peut
  être surchargé via la variable d'environnement `HOST`.

- en mode autonome, le répertoire d'installation par défaut est `~/.kresus/`.
  Cela peut être surchargé via la variable d'environnement `KRESUS_DIR`.

## Recommandations pour le pare-feu

Vous devrez définir les autorisations suivantes :

- Accès http/https au site web de votre banque, pour récupérer les nouvelles
  opérations.
- Accès http/https aux dépôts Weboob, pour la mise à jour automatique des
  modules avant les rappatriements automatiques.
